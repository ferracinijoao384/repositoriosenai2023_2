const { createApp} = Vue;

createApp({
    data(){
        return{
            username:"",
            password:"",
            error: null,
            sucesso: null,
            userAdmin: false,

            //Variaveis para armazenamento das entradas do formulario da pagina de cadastro
            newUsername:"",
            newPassword:"",
            confirmPassword:"",

            //Declaraçao dos arrays para armazenamento de usuarios e senhas
            usuarios:["admin" , "Joao" , "cristiano"],
            senhas:["1234","1234","1234"],
            mostrarEntrada: false,
            mostrarLista: false, 
        }
    },//fechamento data

    methods:{
        login(){
            setTimeout(() => {
                //alert("Dentro do setTimeout!")
                if((this.username === "Joao" && this.password === "12345678") ||
                (this.username === "Cristiano" && this.password === "12345678") ||
                  (this.username === "Admin" && this.password === "admin12345" )){
                    this.sucesso = "Login efetuado com sucesso!";
                    this.error = null;
                    if(this.username === "Admin"){
                        this.userAdmin = true;
                    }
                }
                else{
                    //alert("login não efetuado");
                    this.error = "Nome ou senha incorretos!";
                    this.sucesso = null;
                }
            }, 1000);
            //alert("Saiu do steTimeou!!!");
        },//Fechamento login

        login2(){
            this.mostrarEntrada = false;

            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }

            setTimeout(() => {
                this.mostrarEntrada = true;

                const index = this.usuarios.indexOf(this.username);
                if(index !== -1 && this.senhas[index] === this.password){
                   
                    this.sucesso = "Login efetuado com sucesso";
                    localStorage.setItem("username", this.username);
                    localStorage.setItem("password", this.password);

                    this.error = null;
                    this.sucesso = "Login efetuado com sucesso";
                    
                    if(this.username === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "Logado como ADMIN!!!";
                    }
                }//fechamento if 
                else{
                    this.error = "Nome ou senha incorretos!";
                    this.userAdmin = true;
                    this.sucesso = "Logado como ADMIN!!!";
                }

                this.username ="";
                this.password ="";

            }, 500);
                 
        },//fechamento login2

        paginaCadastro(){
            this.mostrarEntrada = false;
            if(this.userAdmin == true){
                this.sucesso="Login ADM identificado!";
                this.error = null;
                this.mostrarEntrada = true;

                //localStorage.setItem("username", this.username);
               //localStorage.setItem("password", this.password);

                setTimeout(() => {}, 2000);

                setTimeout(() =>{
                    window.location.href = "paginaCadastro.html";
                },1000);//fechamento setTimeout

            }
            else{
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null; 
                    this.error = "faça login com usuario administrativo!";
                },500);
            }//fechamento setTimeout

        },//fechamento paginaCadastro

        adicionarUsuario(){
            this.username = localStorage.getItem("username");
            this.password = localStorage.getItem("password");            

            this.mostrarEntrada = false; 

            setTimeout(() => {
                this.mostrarEntrada = true;
                if(this.username === "admin"){
                    if(this.newUsername !== ""){
                        if(!this.usuarios.includes(this.newUsername)){
                          if(this.newPassword && this.newPassword === this.confirmPassword){
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);

                            //armazenamento do ususario e senha no localStorage

                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));

                            this.error = null;
                            this.sucesso = "Usuário cadastrado com sucesso!";
                            }//fechamento do if 
                            else{
                                this.sucesso = null;
                                this.error = "Por favor, confirme sua senha!!!";
                            }

                        }//fechamento if incluides 
                        else{  
                            this.sucesso = null;
                            this.error = "O usuário informado já está cadastrado";
                        }
                    }//Fechamento if !== ""
                    else{
                        this.error = "Por favor, digite um nome de usuario!";
                        this.sucesso = null;
                    }//fechamento do else
                    
                }
                else{
                    this.error ="Usuario Não ADM!!!";
                    this.sucesso = null;
                }

                this.newUsername ="";
                this.newPassword ="";
                this.confirmPassword ="";

            },500);
        },//Fechamento adicionarUsuarios

        verCadastro(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }//Fechamento if
            this.mostrarLista = !this.mostrarLista;
        },//fechamento verCadastrados
        
        excluirUsuario(usuario){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
                //Impedir a exclusão do usuarios admin
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.error = "o usuario ADMIN nao pode ser excluidos!!!";

                }, 500);
                return;//força a finalizaçao do bloco / funçao 
            }//fechamento if usuario

            if(confirm("tem certeza que deseja excluir o usuario?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    
                    //atualizaçao dos vetores localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));

                }//fechamento index

            }//excluir do if confirm
        },//fechamento excluirUsuarios
    },//Fechamento methods

}).mount("#app");