const {createApp} = Vue;

createApp({
    data(){
        return{
            display:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,
        }
},//Fechamento data

    methods:{
        numero(valor){
            if(this.display == 0){
                this.display= valor.toString();
            }
            else{
                this.display = this.display + valor.toString();
            }
            
        },//fechamento numero
        decimal(){
            if(!this.display.includes(".")){
                this.display = this.display + ".";
                }
        },//fechamento 

        clear(){
            this.display = "0";
            this.numeroAnterior = null;
            this.numeroAtual = null;
            this.operador = null;
        },//fechamento clear
        
    },//Fechamento methods

}).mount("#app");//Fechamento createApp