const {createApp} = Vue;

createApp({
    data(){
        return{
            display:"0",
            operador:null,
            numeroAtual: null,
            numeroAnterior: null,
            tamanhoDisplay: 10,
        };
    },//Fechamento data

    methods:{
        numero(valor){            
            if(this.display == "0"){
                this.display = valor.toString();
            }

            else if(this.display.length >= this.tamanhoDisplay){
                alert("Limete do Display atigindo!");
                
            }

            else{

                if(this.operador == "="){
                    this.display = '';
                    this.operador = null;
                }

                //this.display = this.display + valor.toString();

                //Fórmula resumida:
                this.display += valor.toString();
            }
        },//Fechamento numero
        
        decimal(){
            if(!this.display.includes(".")){
                //this.display = this.display + ".";

                //Fórmula resumida:
                this.display += ".";
            }
        },//Fechamento 

        clear(){
            this.display = "0";
            this.numeroAtual = null;
            this.numeroAnterior = null;
            this.operador = null;
        },//Fechamento clear

        operacoes(operacao){
            if(this.operador != null){
                const displayAtual = parseFloat(this.display);
                switch(this.operador){
                    case "+":
                        this.display = (this.numeroAtual + displayAtual).toString();
                        break;
                    case "-":
                        this.display = (this.numeroAtual - displayAtual).toString();
                        break;
                    case "*":
                        this.display = (this.numeroAtual * displayAtual).toString();
                        break;
                    case "/":
                        this.display = (this.numeroAtual / displayAtual).toString();
                        break;
                }//Fim do switch
                this.numeroAnterior = this.numeroAtual;
                this.numeroAtual = null;
                this.operador = null;
            }//Fim do if

            if(operacao  != "="){
                this.operador = operacao;
                this.numeroAtual = parseFloat(this.display);
                this.display = "0";
            }
            else{
                this.operador = operacao;
                if(this.display.length >= this.tamanhoDisplay){
                    alert("Limete do Display atigindo!");
                    
                }

                if(this.display.incluides(".")){
                    const displayAtual= parseFloat(this.display);
                    this.display = (displayAtual.toFixed(2)).toString();
                }
            }
            
        }
        
    },//Fechamento methods



}).mount("#app");//Fechamento createApp